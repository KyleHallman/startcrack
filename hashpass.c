#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "md5.h"


int main(int argc, char *argv[])
{
    int start;
    int end;
    int count = 0;

    if (argc < 3)
    {
        printf("Must supply a src and dest file\n");
        exit(1);
    }
    //open first file for reading
    FILE *src = fopen(argv[1], "r"); 
    if (!src)
    {
        printf("Cant open %s for reading\n", argv[1]);
        exit(1);
    }

    //open second file for writing
    FILE *dest = fopen(argv[2], "w");
    if (!dest)
    {
        printf("cant open %s for writing\n", argv[2]);
        exit(2);
    }

    //loop through first file, writing data for second as we go.
    char file[100];
    while (fgets(file, 100, src) != NULL)
    {
        file[strlen(file)-1] = '\0';
        char *hash = md5(file, strlen(file));
        //printf("%s\n", hash);
        fprintf(dest, "%s \n", hash);
 
    }
    fclose(src);
    fclose(dest);
}